package com.example.trafficlightsmap;

import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;

public class TimerDialogue extends DialogFragment {
    private TextView mCountdownView;
    private long counter;
    boolean curState; // 0 is red, 1 is green
    int greenCycle = 30000;
    int redCycle = 60000;
    ModifiableCountDownTimer lightTimer;
    private LayoutInflater inflater;
    private ViewGroup container;
    public TimerDialogue(){counter = 10000; curState = true;}

    public void setTime(long counter) {
        this.counter = counter;
    }
    public void setCurState(boolean state) {
        this.curState = state;
    }
    public void setCycle(int red, int green) {
        this.greenCycle = green;
        this.redCycle = red;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.green_time, container);
        this.inflater = inflater;
        this.container = container;
        mCountdownView = view.findViewById(R.id.countdownTimerGreen);
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        lightTimer.cancel();
    }

    @Override
    public void onActivityCreated(final Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Dialog dialog = getDialog();
        WindowManager.LayoutParams lp = dialog.getWindow().getAttributes();
        lp.dimAmount=0.0f;
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setAttributes(lp);
        dialog.getWindow().getAttributes().gravity = Gravity.BOTTOM;
        dialog.setCanceledOnTouchOutside(true);
        lightTimer = new ModifiableCountDownTimer(counter, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                if(curState) {
                    mCountdownView.setTextColor(Color.parseColor("#DC143C"));
                } else {
                    mCountdownView.setTextColor(Color.parseColor("#32CD32"));
                }

                mCountdownView.setText("                     "+((int)Math.round(millisUntilFinished/1000.0)-1));
                mCountdownView.setGravity(Gravity.END);
            }

            @Override
            public void onFinish() {

                if(curState) {
                    curState = false;
                    counter = (long)redCycle;
                }
                else {
                    curState = true;
                    counter = (long)greenCycle;
                }
                System.out.println(counter);
                this.setMillisInFuture(counter);
                this.start();
        }};

        lightTimer.start();
        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                lightTimer.cancel();
            }
        });

    }
}
