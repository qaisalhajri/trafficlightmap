package com.example.trafficlightsmap;
import java.time.LocalTime;
import java.time.ZoneId;

public class TimerCycle {
    private int redCycle = 60;
    private int greenCycle = 30;
    private int totalCycle = 90;
    public TimerCycle(){}

    public TimerCycle(int red, int green) {
        this.redCycle = red;
        this.greenCycle = green;
        this.totalCycle = this.redCycle + this.greenCycle;
    }
    public void setRedCycle(int red) {
        this.redCycle = red;
        this.totalCycle = this.redCycle + this.greenCycle;
    }
    public void setGreenCycle(int green) {
        this.greenCycle = green;
        this.totalCycle = this.redCycle + this.greenCycle;
    }
    public int getRedCycle() {return this.redCycle;}

    public int getGreenCycle() {return this.greenCycle;}

    public boolean getCurState() {
        boolean state = true;
        LocalTime now = LocalTime.now(ZoneId.systemDefault());
        int cur_time = now.toSecondOfDay();

        if(cur_time % totalCycle <= redCycle) {
            state = false;
        } else {
            state = true;
        }

        return state;
    }
    public int getTimeRemaining() {
        LocalTime now = LocalTime.now(ZoneId.systemDefault());
        int cur_time = now.toSecondOfDay();
        int time_remaining = 0;
        int curCycleTime = cur_time % totalCycle;
        if(curCycleTime <= redCycle) {
            time_remaining = redCycle - curCycleTime;
        } else {
            time_remaining = totalCycle - curCycleTime;
        }
        return time_remaining;

    }

}
