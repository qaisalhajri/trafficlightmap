package com.example.trafficlightsmap;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;

public class MapsActivity extends FragmentActivity implements
        OnMapReadyCallback,
        OnMarkerClickListener {
    private GoogleMap mMap;
    private static final LatLng OMAN = new LatLng(23.5859, 58.4059);
    private static final LatLng BLACKSBURG = new LatLng(37.2296, -80.4139);
    private static LatLng CURLOC;
    List<String[]> rows = new ArrayList<>();
    private TimerCycle lightCycle;
    private Map<Marker, TimerCycle> lightData = new HashMap<>();


    private TimerDialogue countDown;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        lightCycle = new TimerCycle();
        countDown = new TimerDialogue();


        InputStream is = getResources().openRawResource(R.raw.traffic_info);
        BufferedReader reader = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8"))) ;
        String line;
        try {
            while ((line = reader.readLine()) != null) {
                String[] tokens = line.split(",");
                rows.add(tokens);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        CURLOC = new LatLng(Double.parseDouble(rows.get(1)[2]), Double.parseDouble(rows.get(1)[3]));


        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }



    @Override
    public boolean onMarkerClick(Marker marker){
        Fragment fragment = getSupportFragmentManager().findFragmentByTag("fragment_countdownTimer");
        if(fragment != null) {
            TimerDialogue td = (TimerDialogue) fragment;
            td.dismiss();
        }
        System.out.println(marker.getTitle());
        lightCycle = lightData.get(marker);
        System.out.println(lightCycle.getTimeRemaining());
        System.out.println(lightCycle.getGreenCycle());
        System.out.println(lightCycle.getRedCycle());
        countDown.setCurState(lightCycle.getCurState());
        countDown.setCycle(lightCycle.getRedCycle()*1000, lightCycle.getGreenCycle()*1000);
        countDown.setTime(lightCycle.getTimeRemaining() * 1000);
        countDown.show(getSupportFragmentManager(), "fragment_countdownTimer");
        return false;
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    void dataToMap() {
        for (String[] t : rows) {
            String lightID = t[0];
            String lightName = t[1];
            double lat = Double.parseDouble(t[2]);
            double lon = Double.parseDouble(t[3]);
            int red = Integer.parseInt(t[4]);
            int green = Integer.parseInt(t[5]);
            Marker mark = mMap.addMarker(new MarkerOptions()
            .position(new LatLng(lat, lon))
            .title(lightName)
            .snippet("Intersection #" + lightID));
            lightData.put(mark, new TimerCycle(red, green));
        }
    }



    @Override
    public void onMapReady(GoogleMap googleMap) {
        //
        mMap = googleMap;
        System.out.println(CURLOC);
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(/*new LatLng(37.7807,-122.4724)*/CURLOC, 15), 1000, null);
        // Handles all mapping data from file
        dataToMap();

        mMap.setOnMarkerClickListener(this);
        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng arg0) {
                Fragment fragment = getSupportFragmentManager().findFragmentByTag("fragment_countdownTimer");
                if(fragment != null) {
                    countDown.dismiss();
                }
                Log.d("arg0", arg0.latitude + "-" + arg0.longitude);
            }
        });

        }

    private static int index_of_minimum_value(float[] array) {
        int index = 0;
        for (int i = 1; i < array.length; i++) {
            if ((array[i - 1] < array[i]) && (array[index] > array[i - 1])) index = i - 1;
        else if (array[index] > array[i]) index = i;
        }
        return index;
    }
}
